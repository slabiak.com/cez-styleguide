const React = require('react');
const Styles = require("./components/Styles.css")

const VerticalPositioning = require('./components/VerticalPositioning');
const HorizontalPositioning = require('./components/HorizontalPositioning');
const TablePositioning = require('./components/TablePositioning');
const RepairComponent = require('./components/RepairComponent');
const DateAndTime = require('./components/DateAndTime');
const WriteFromArray = require('./components/WriteFromArray');
const WriteFromADatabase = require('./components/WriteFromADatabase');
const ShadowCreator = require('./components/ShadowCreator');
const ColorsChecker = require('./components/ColorsChecker');


class App extends React.Component {
    constructor(props) {
        super(props);

        this.panel = React.createRef();
    }

    render() {
        return (
            <div>
                <ColorsChecker />
                <p className="large"><b>Korekta paddingów</b></p>
                <div className={Styles.border}>

                    <p >Komponenty uporządkują się od góry do dołu lub od lewej do prawej</p>
                    <VerticalPositioning />

                    <HorizontalPositioning />
                </div>
                <p className="large"><b>Tworzenie tabeli</b></p>
                <div className={Styles.border}>

                    <TablePositioning />
                </div>
                <p className="large"><b>Lorem ipsum</b></p>
                <div className={Styles.border}>
                    <p>Czas</p>
                    <DateAndTime/>
                </div>
                <div className={Styles.border}>
                    <p>Autouzupełnianie</p>
                    <WriteFromADatabase/>
                </div>
                
                <div className={Styles.border}>
                    <p>Losuj. Wartości rozdzielone znakiem $</p>
                    <WriteFromArray/>
                </div>
                
                <div className={Styles.border}>
                <ShadowCreator />
                </div>
                <RepairComponent />

            </div>
        );
    }
}

module.exports = App;
