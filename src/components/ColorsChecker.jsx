const React = require("react");
const ReactDOM = require("react-dom");


class ColorsChecker extends React.Component {



  constructor() {
    super();
    this.state = {
      numberOfItemsWithWrongColors__fill: 0,
      numberOfItemsWithWrongColors__border: 0,
      weAreLookingFor: [
        //yellow
        "fffaf5ed", "ffffdf89", "ffffd566", 'ffffc33f', 'fffea700', 'ffff9d00', 'ffb76b00', 'ff935100', 'ff7a3e00',
        //green
        'ffe3f2e8', 'ffa5f8a5', 'ff75eb80', 'ff50d86a', 'ff1fbf4d', 'ff16a44d', 'ff0f894a', 'ff096e44', 'ff055b40',
        //blue
        'fff7fafc', "fff0f6fa", 'ff78bada', 'ff3196c7', 'ff0064a3', 'ff044d8c', 'ff17345d', 'ff01295e', 'ff0b1e39',
        //red
        'fffff9f7', 'fffac396', 'fff19761', 'ffe36d39', 'ffd13101', 'ffd11632', 'ffa30000', 'ff790200', 'ff640005',
        //grey
        'fffcfdff', 'fffafafa', 'ffd0d1d3', 'ffb6b7b8', 'ff6f7181', 'ff5c5d69', 'ff444444', 'ff2e2e33','ff1b1b1f',
        //others
        'ffffffff', 'ff000000','ffffe3f4'
      ],
      toEdit: []
    };

    this.verifyColors = this.verifyColors.bind(this);
    this.checkItem = this.checkItem.bind(this);
    this.iterateThroughElements = this.iterateThroughElements.bind(this);
  }



  iterateThroughElements(items) {
    items.forEach((element) => {
      this.checkItem(element)
    })
  }

  checkItem(element) {
    if (element.fill != undefined) {
      if (element.fillEnabled == true) {
        let obj = element.fill

        if (obj.value != undefined) {
          let c = obj.value.toString(16)

          if (!this.state.weAreLookingFor.includes(c) == true) {
            console.log("fill")
            console.log(element)

            this.setState(prevState => ({ numberOfItemsWithWrongColors__fill: prevState.numberOfItemsWithWrongColors__fill + 1 }))

            this.forceUpdate();
          }
        }
      }
    }
    if (element.stroke != undefined) {
      if (element.strokeEnabled == true) {
        let str = element.stroke
        if (str.value != undefined) {
          let d = str.value.toString(16)
          if (!this.state.weAreLookingFor.includes(d) == true) {
            console.log("border")
            console.log(element)


            this.setState(prevState => ({ numberOfItemsWithWrongColors__border: prevState.numberOfItemsWithWrongColors__border + 1 }))
            this.forceUpdate();
          }
        }
      }
    }
    this.iterateThroughElements(element.children)
  }



  verifyColors() {
    const { editDocument } = require("application");
    const { selection } = require("scenegraph");


    this.setState({
      numberOfItemsWithWrongColors__fill: 0,
      numberOfItemsWithWrongColors__border: 0
    })

    editDocument({ editLabel: "Change position" },
      () => {
        this.iterateThroughElements(selection.items)
      })


    // let dialog = document.createElement('dialog');
    // ReactDOM.render(<form method="dialog">
    //   <h1>Ktoś tu oszukuje z kolorkami!
    //   </h1>
    //   <label>Elementy spoza przyjętej palety barw</label>
    //   <p><b>{this.state.numberOfItemsWithWrongColors__fill}</b> fill</p>
    //   <p><b>{this.state.numberOfItemsWithWrongColors__border}</b> border</p>
    //   <p >Proszę stosować sie do styleguida!</p>
    //   <footer>
    //     <button type="submit" uxp-variant="cta">OK, zaraz to poprawię</button>
    //   </footer>
    // </form>, dialog)
    // document.body.appendChild(dialog).showModal();

  }





  render() {
    return (
      <div>

        <button onClick={this.verifyColors}>Zweryfikuj kolory</button>
      </div>
    );
  }
}

module.exports = ColorsChecker;