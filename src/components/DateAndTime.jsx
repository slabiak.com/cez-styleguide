const React = require("react");
const Styles = require('./Styles.css')

function randomInt(min, max) {
  return min + Math.floor((max - min) * Math.random());
}



function writeDate() {
  const { editDocument } = require("application");
  const { selection } = require("scenegraph");
  editDocument({ editLabel: "Change name" },
    () => {
      selection.items.forEach((element) => {
        if (element.constructor.name == "Text") {
          let day = randomInt(1, 27)
          if (day < 10) {
            day = "0" + day
          }
          let month = randomInt(1, 12)
          if (month < 10) {
            month = "0" + month
          }
          element.text = day +"."+month+".2020"
        }
      })
    })
}
function writeTime() {
  const { editDocument } = require("application");
  const { selection } = require("scenegraph");
  editDocument({ editLabel: "Change name" },
    () => {
      selection.items.forEach((element) => {
        if (element.constructor.name == "Text") {
          let hour = randomInt(1, 24)
          if (hour < 10) {
            hour = "0" + hour
          }
          let minutes = randomInt(0, 60)
          if (minutes < 10) {
            minutes = "0" + minutes
          }
          element.text = hour + ":" + minutes
        }
      })
    })
}
function writeDateAndTime() {
  const { editDocument } = require("application");
  const { selection } = require("scenegraph");
  editDocument({ editLabel: "Change name" },
    () => {
      selection.items.forEach((element) => {
        if (element.constructor.name == "Text") {
          let day = randomInt(1, 27)
          if (day < 10) {
            day = "0" + day
          }
          let month = randomInt(1, 12)
          if (month < 10) {
            month = "0" + month
          }

          let hour = randomInt(1, 24)
          if (hour < 10) {
            hour = "0" + hour
          }
          let minutes = randomInt(0, 60)
          if (minutes < 10) {
            minutes = "0" + minutes
          }
          element.text =day +"."+month+".2020, "+ hour + ":" + minutes
        }
      })
    })
}

const DateAndTime = () => (
  <div>
    <button onClick={writeDate}>Date</button>
    <button onClick={writeTime}>Time</button>
    <button onClick={writeDateAndTime}>Date + time</button>
  </div>
);

module.exports = DateAndTime;