const React = require("react");
const  Styles = require('./Styles.css')

function comparePositions_X(a, b) {
  return a.globalBounds.x - b.globalBounds.x
}

function reposition() {
  const { editDocument } = require("application");
  const { selection } = require("scenegraph");
  editDocument({ editLabel: "Change position" },
    () => {
      selection.items = selection.items.sort(comparePositions_X)
      let currentItem;
      selection.items.forEach(element => {
        const newPosition = calculateNewPosition(currentItem, element)
        const oldfPosition = getOldPosition(currentItem, element)
        element.placeInParentCoordinates(oldfPosition, newPosition)     //    element.resize(350, element.globalBounds.height)
        currentItem = element
      })
    })
}

function getOldPosition(currentItem, newItem) {
  let artboardPosition = newItem.parent
  if (currentItem == undefined) return { x: -newItem.globalBounds.x + artboardPosition.globalBounds.x, y: -newItem.globalBounds.y + artboardPosition.globalBounds.y }
  return { x: -currentItem.globalBounds.x + artboardPosition.globalBounds.x, y: -currentItem.globalBounds.y + artboardPosition.globalBounds.y}

}


function calculateNewPosition(currentItem, newItem) {
  if (currentItem == undefined) return { x: "0", y: "0" }
  return { x: currentItem.globalBounds.width + 24, y: "0" }
}


const HorizontalPositioning = () => (
  <div className={Styles.flex}>
    <button onClick={reposition}>Ustaw komponenty w poziomie</button>
  </div>
);

module.exports = HorizontalPositioning;