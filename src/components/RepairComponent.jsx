const React = require("react");


class RepairComponent extends React.Component {
  constructor() {
    super();
    this.state = {
      toEdit: []
    };
    this.repair = this.repair.bind(this);
    this.resize = this.resize.bind(this);
    this.repairElement = this.findTextsInsideComponent.bind(this);

  }

  repair() {
    const { editDocument } = require("application");
    const { selection, Color } = require("scenegraph");




    editDocument({ editLabel: "Change position" },
      () => {
        selection.items.forEach((element) => {


          if (element.constructor.name == "Text") {

            let currentText = element.text
            if (currentText.slice(-1) != "$") element.text = currentText + "$"
            if (currentText.slice(-1) == "$") element.text = currentText.slice(0, -1);
          }


          if (element.name == "wizard_group") {

            let numberOfArrows = 0
            let numberOfElements = 0
            element.children.forEach(e => {
              if (e.name == "icon / arrow / left" || e.name == "icon / arrow / right") {
                numberOfArrows++
              }
              if (e.name == "* / wizard / element") {
                numberOfElements++
              }

            });


            element.children.forEach(e => {
              if (e.name == "* / wizard / element") {
                e.resize((1546 - 34 * numberOfArrows) / numberOfElements, e.globalBounds.height)
              }
            });


          }

    

          
          if (element.name == "timeRange_group") {
            let width = element.parent.localBounds.width
            element.children.forEach(e => {
              if (e.name == "* / textfield / small / 2 values") {
                e.resize((width - 24) / 2, e.globalBounds.height)
              }
              else{
                e.resize(8, e.globalBounds.height)
              }
            });
          }
          if (element.name == "tab_group") {
            let dividerPosition
            let newPosition

            element.parent.children.forEach(child => {
              if (child.name == "divider") {
                dividerPosition = { x: child.localBounds.x, y: child.localBounds.y }
                newPosition = { x: "0", y: "-24", }
              }
            });

            element.parent.children.forEach(child => {
              if (child.name == "tab_group") {
                console.log(dividerPosition)
                console.log(newPosition)
                child.placeInParentCoordinates(dividerPosition, newPosition)
              }
            });
          }

          if (element.constructor.name == "Artboard") {
            let lowestPoint

            element.fill = new Color("#F5F5F5");
            if (element.width > "1000" && element.height < "1080") {

              element.height = 1080

            }

          }
        })


      })



  }


  resize() {
    const { editDocument } = require("application");
    const { selection } = require("scenegraph");

    editDocument({ editLabel: "Change position" },
      () => {
        this.state.toEdit = []
        selection.items.forEach((element) => {
          this.findTextsInsideComponent(element)
        })

      })
  }

  findTextsInsideComponent(element) {
    if (element.constructor.name == "Text") {
      element.text = "test\nptest"

    }
    element.children.forEach((childNode, i) => {
      this.findTextsInsideComponent(childNode)

    });

  }

  render() {
    return (
      <div>

        <button onClick={this.repair}>Napraw komponent</button>
      </div>
    );
  }
}

module.exports = RepairComponent;