const React = require("react");
const Styles = require('./Styles.css')

class ShadowCreator extends React.Component {
  constructor() {
    super();
    this.state = {

    };
    this.setShadow = this.setShadow.bind(this);

  }



  setShadow(level) {
    const { editDocument } = require("application");
    const { selection, Shadow, Color } = require("scenegraph");


    editDocument({ editLabel: "Change shadow" },
      () => {
        selection.items.forEach((element) => {
          // console.log(element)
          let shadowColor
          let blur = 0
          let shadowStep = 3
          let opacityStep = 0.03
          if (level == 0) {
            shadowColor = new Color('#000000', 0.0); // 0.3 opacity
            blur = 0
          }
          else  {
            shadowColor = new Color('#000000', 0.1 + level*opacityStep); // 0.3 opacity
            blur = level * shadowStep
          }

          element.shadow = new Shadow(0, 2, blur, shadowColor, true)
        })

      })
  }



  render() {


    return (
      <div className={Styles.flex}>

        <p>Ustaw elewacje</p>
        <button  onClick={ () => this.setShadow(0)}>Elewacja nr 0</button>
        <button  onClick={ () => this.setShadow(1)}>Elewacja nr 1</button>
        <button  onClick={ () => this.setShadow(2)}>Elewacja nr 2</button>
        <button  onClick={ () => this.setShadow(3)}>Elewacja nr 3</button>
        <button  onClick={ () => this.setShadow(4)}>Elewacja nr 4</button>
        <button  onClick={ () => this.setShadow(5)}>Elewacja nr 5</button>
      </div>
    );
  }

}
module.exports = ShadowCreator;