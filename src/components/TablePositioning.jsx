const React = require("react");
const Styles = require('./Styles.css')

class TablePositioning extends React.Component {
  constructor() {
    super();
    this.state = {
      artboardGridWidth: 157,
      artboardGridGap: 24,
      predefinedWidth: 1,
      groupLevel: 0,
      rowDividers: "1",
      columns: 12,
      FirstAndLastGap: 64,
      gridSpace: 1546
    };

    this.changeArtboardGridColumns = this.changeArtboardGridColumns.bind(this);
    this.changeColumnsNumber = this.changeColumnsNumber.bind(this);
    this.resize = this.resize.bind(this);
    this.comparePositions_X = this.comparePositions_X.bind(this)
    this.getOldPosition = this.getOldPosition.bind(this)
    this.calculateNewPosition = this.calculateNewPosition.bind(this)
    this.changeFirstAndLastGapSize = this.changeFirstAndLastGapSize.bind(this)
    this.changeGroupLevel = this.changeGroupLevel.bind(this)
    this.changeGridSpace = this.changeGridSpace.bind(this)
    this.countFieldSizeInTable = this.countFieldSizeInTable.bind(this)
    this.countInnerPaddingInTable = this.countInnerPaddingInTable.bind(this)
    this.countExternalPaddingInTable = this.countExternalPaddingInTable.bind(this)
    this.getPredefinedWidth = this.getPredefinedWidth.bind(this)
    this.getSizeOfOneColumn = this.getSizeOfOneColumn.bind(this)
    this.getSizeOfChoosenField = this.getSizeOfChoosenField.bind(this)
    
  }

  getOldPosition(currentItem, newItem) {
    let artboardPosition = newItem.parent
    if (currentItem == undefined) return { x: -newItem.globalBounds.x + artboardPosition.globalBounds.x, y: -newItem.globalBounds.y + artboardPosition.globalBounds.y }
    return { x: -currentItem.globalBounds.x + artboardPosition.globalBounds.x, y: -currentItem.globalBounds.y + artboardPosition.globalBounds.y }

  }


  calculateNewPosition(currentItem, newItem) {
    if (currentItem == undefined) return { x: "0", y: "0" }
    return { x: currentItem.globalBounds.width + 24, y: "0" }
  }

  comparePositions_X(a, b) {
    return a.globalBounds.x - b.globalBounds.x
  }

  changeColumnsNumber(e) {
    this.setState({ columns: e.target.value });
  }

  changeArtboardGridColumns(e) {
    this.setState({ predefinedWidth: e.target.value });
  }
  changeFirstAndLastGapSize(e) {
    this.setState({ FirstAndLastGap: e.target.value });
  }
  changeGroupLevel(e) {
    this.setState({ groupLevel: e.target.value });
  }
  changeRowDividers(e) {
    this.setState({ rowDividers: e.target.value });
  }
  changeGridSpace(e) {
    this.setState({ gridSpace: e.target.value });
  }


  getPredefinedWidth() {
   let a = this.state.gridSpace
    switch (this.state.predefinedWidth) {
      case '0': return a = this.state.gridSpace
      case '1': return a = 1546
      case '2': return a = 1482
    }
    return a
  }



  countFieldSizeInTable(columnWidth) {
    return this.getPredefinedWidth() / this.state.columns * columnWidth
  }

  countInnerPaddingInTable(elementsCount) {

    return ((elementsCount - 1) * this.state.artboardGridGap) / (elementsCount)
  }

  countExternalPaddingInTable(elementsCount) {
    return this.state.FirstAndLastGap / (elementsCount)
  }

  getSizeOfOneColumn() {
    return (this.getPredefinedWidth() - this.state.FirstAndLastGap - (this.state.artboardGridGap * (this.state.columns-1)) )/ this.state.columns
  }

  getSizeOfChoosenField(numberOfColumns) {  
    return numberOfColumns * (this.getSizeOfOneColumn()) + this.state.artboardGridGap * (numberOfColumns-1)
  }


  resize() {
    const { editDocument } = require("application");
    const { selection } = require("scenegraph");
    let columnWidths = this.state.rowDividers.split(" ")
  
    let i = 0
    let j = 0


    editDocument({ editLabel: "Change position" },
      () => {

        selection.items = selection.items.sort(this.comparePositions_X);
 

        selection.items.forEach(element => {
          let columnWidth = columnWidths[i]

          element.resize(this.getSizeOfChoosenField(columnWidth), element.globalBounds.height)
          i += 1
          if (i == columnWidths.length) {
            i = 0
          }
        })
        let currentItem;
        selection.items.forEach(element => {
          const newPosition = this.calculateNewPosition(currentItem, element)
          const oldPosition = this.getOldPosition(currentItem, element)
          element.placeInParentCoordinates(oldPosition, newPosition)     //    element.resize(350, element.globalBounds.height)
          currentItem = element
        })
      })
  }



  render() {



    return (
      <div className={Styles.flex}>

        <p>W środku lub poza komponentem</p>
        <select value={this.state.FirstAndLastGap} onChange={(e) => { this.changeFirstAndLastGapSize(e) }}>
          <option value="64">W środku komponentu</option>
          <option value="0">Poza komponentem</option>
        </select>

        <p>Zgrupowane po</p>
        <select value={this.state.groupLevel} onChange={(e) => { this.changeGroupLevel(e) }}>
          <option value="0">0 parametrów</option>
          <option value="40">1 parametr</option>
          <option value="80">2 parametry</option>
          <option value="120">3 parametry</option>
        </select>


        <p>Wybierz ile kolumn z grida (artboard) zajmie tabela</p>
        <select value={this.state.predefinedWidth} onChange={(e) => { this.changeArtboardGridColumns(e) }}>

          <option value="1">table / record - 8 kolumn</option>
          <option value="2">accordion</option>
          <option value="0">własna szerokość</option>
        </select>





        { this.state.predefinedWidth == "0" ? <div><p>Wpisz szerokość dla której tworzyć grid</p>
          <input value={this.state.gridSpace} onChange={(e) => { this.changeGridSpace(e) }}></input>
        </div> : null}

        <p>Wpisz moduły wg których będzie ustalana szerokość komponentów, Moduły odzielaj spacją</p>
        <input value={this.state.rowDividers} onChange={(e) => { this.changeRowDividers(e) }}></input>

        <p>Max Liczba modułów w Gridzie </p>
        <input value={this.state.columns} onChange={(e) => { this.changeColumnsNumber(e) }}></input>



        <button onClick={this.resize}>Rozmieszcz pola w oparciu o grid</button>
      </div>
    );
  }

}
module.exports = TablePositioning;