const React = require("react");
const Styles = require('./Styles.css')

class VerticalPositioning extends React.Component {

  constructor() {
    super();

    this.state = {
      alignToLeft: true
    };


    this.comparePositions_Y = this.comparePositions_Y.bind(this);
    this.reposition = this.reposition.bind(this);
    this.getOldPosition = this.getOldPosition.bind(this);
    this.calculateNewPosition = this.calculateNewPosition.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.generate_X_Value = this.generate_X_Value.bind(this);

  }

  comparePositions_Y(a, b) {
    return a.globalBounds.y - b.globalBounds.y
  }


  reposition() {
    const { editDocument } = require("application");
    const { selection } = require("scenegraph");
    editDocument({ editLabel: "Change position" },
      () => {
        selection.items = selection.items.sort(this.comparePositions_Y)
        let currentItem;
        selection.items.forEach(element => {
          const newPosition = this.calculateNewPosition(currentItem, element)
          const oldPosition = this.getOldPosition(currentItem, element)


          element.placeInParentCoordinates(oldPosition, newPosition)     //    element.resize(350, element.globalBounds.height)
          currentItem = element
        })
      })
  }

  getOldPosition(currentItem, newItem) {
    let artboardPosition = newItem.parent
    if (currentItem == undefined) return { x: -newItem.globalBounds.x + artboardPosition.globalBounds.x, y: -newItem.globalBounds.y + artboardPosition.globalBounds.y }
    return { x: this.generate_X_Value(currentItem, newItem) + artboardPosition.globalBounds.x, y: -currentItem.globalBounds.y + artboardPosition.globalBounds.y }

  }

  generate_X_Value(currentItem, newItem) {

    if (this.state.alignToLeft == true) {
      return -currentItem.globalBounds.x
    } else {
      return -newItem.globalBounds.x
    }

  }

  calculateNewPosition(currentItem, newItem) {

    if (currentItem == undefined) return { x: "0", y: "0" }



    // header
    if (newItem.name == "subheader /  e-translab") return { x: "0", y: currentItem.globalBounds.height }
    if (newItem.name == "subheader / tabs /  e-transplant") return { x: "0", y: currentItem.globalBounds.height }
    if ((currentItem.name == "subheader / tabs /  e-transplant" || currentItem.name == "subheader /  e-translab" || currentItem.name == "header") && newItem.name == "header / breadcrumbs") return { x: "344", y: currentItem.globalBounds.height }
    if ((currentItem.name == "subheader / tabs /  e-transplant" || currentItem.name == "subheader /  e-translab" || currentItem.name == "header") && (newItem.name == "sidenav" || newItem.name == "sidenav / SMK")) return { x: "0", y: currentItem.globalBounds.height }
    if (currentItem.name == "header / breadcrumbs") return { x: "0", y: currentItem.globalBounds.height }

    let headers = ["H2","H3","H4"]

 
    //footer

    if (newItem.name == "footer / actions") return { x: "0", y: currentItem.globalBounds.height }



    //Table Header
    if (newItem.name == "list / header") return { x: "0", y: currentItem.globalBounds.height + 48 }
    if (currentItem.name == "list / header" && newItem.name == "list / record") return { x: "0", y: currentItem.globalBounds.height }
    if (currentItem.name == "list / header" && newItem.name.includes("accordion") == true) return { x: "0", y: currentItem.globalBounds.height }
    //Table content
    if ((currentItem.name.includes("accordion") == true || currentItem.name == "list / record") && (newItem.name.includes("accordion") == true || newItem.name == "table / record")) return { x: "0", y: currentItem.globalBounds.height + 8 }

    //Table groups
    if ((currentItem.name.includes("accordion") == true || currentItem.name == "list / record") && (newItem.name == "* / list / group")) return { x: "0", y: currentItem.globalBounds.height + 0 }
    if ((currentItem.name == "* / list / group") && (newItem.name.includes("accordion") == true || newItem.name == "list / record")) return { x: "0", y: currentItem.globalBounds.height + 0 }





    //Table Footer
    if ((currentItem.name.includes("accordion") == true || currentItem.name == "list / record") && newItem.name == "* / pagination / small") return { x: "0", y: currentItem.globalBounds.height + 16 }


    if (currentItem.name == "card") return { x: "0", y: currentItem.globalBounds.height + 48 }



    // checkbox
    if (currentItem.name == "checkbox" && newItem.name == "checkbox") return { x: "0", y: currentItem.globalBounds.height + 16 }
    if ((currentItem.name == "label / bold" || currentItem.name == "label / standard") && (newItem.name == "checkbox" || newItem.name == "* / checkbox")) return { x: "0", y: currentItem.globalBounds.height + 12 }

    // radiobutton
    if (currentItem.name == "radiobutton" && newItem.name == "radiobutton") return { x: "0", y: currentItem.globalBounds.height + 16 }
    if ((currentItem.name == "label / bold" || currentItem.name == "label / standard") && (newItem.name == "radiobutton" || newItem.name == "* / radiobutton")) return { x: "0", y: currentItem.globalBounds.height + 8 }


    //checkbox / read only
    let checkbox8 = ["checkbox / read only"]
    if (checkbox8.includes(currentItem.name) && checkbox8.includes(newItem.name)) { return { x: "0", y: currentItem.globalBounds.height + 16 } }
    if (currentItem.name == "label / bold" && checkbox8.includes(newItem.name)) { return { x: "0", y: currentItem.globalBounds.height + 12 } }
    

    //Text plain text
    let plainTextArray8 = ["Plain text", "Plain text / bullets"]
    if (plainTextArray8.includes(currentItem.name) && plainTextArray8.includes(newItem.name)) { return { x: "0", y: currentItem.globalBounds.height + 8 } }
    if (headers.includes(currentItem.name) && (plainTextArray8.includes(newItem.name) || newItem.name == "Plain text / list")) { return { x: "0", y: currentItem.globalBounds.height + 16 } }
    if (plainTextArray8.includes(currentItem.name) && headers.includes(newItem.name)) { return { x: "0", y: currentItem.globalBounds.height + 40 } }
  
    if (currentItem.name == "Plain text / list" && newItem.name == "Plain text / list") { return { x: "0", y: currentItem.globalBounds.height + 16 } }
    if (plainTextArray8.includes(currentItem.name) && newItem.name == "Plain text / list") { return { x: "0", y: currentItem.globalBounds.height + 16 } }
   

       //H
       if (newItem.name == "H2") return { x: "0", y: currentItem.globalBounds.height + 32 }
       if (newItem.name == "H3") return { x: "0", y: currentItem.globalBounds.height + 32 }
       if (newItem.name == "H4") return { x: "0", y: currentItem.globalBounds.height + 32 }
  

    //more cases
    return { x: "0", y: currentItem.globalBounds.height + 24 }



  }

  handleChange(event) {
    this.setState({
      alignToLeft: event.target.checked
    });
  }

  render() {
    return (
      <div className={Styles.flex}>
        <label className="row" style={{ alignItems: "center" }}>
          <input type="checkbox" defaultChecked={this.state.alignToLeft} onClick={this.handleChange} />
          <span>Wyrównaj elementy do lewej</span>
        </label>
        <button onClick={this.reposition}>Ustaw komponenty w pionie</button>
      </div>
    );
  }
}

module.exports = VerticalPositioning;