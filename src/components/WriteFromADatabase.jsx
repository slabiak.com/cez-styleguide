const React = require("react");
const Styles = require('./Styles.css')
const Con = require( './labelsAndValues')

class writeFromADatabase extends React.Component {
  constructor() {
    super();
    this.state = {
      label: "",
      value: ""
    };
    this.readFromFiles = this.readFromFiles.bind(this)
    this.chooseRandomlyBasingOnLabel = this.chooseRandomlyBasingOnLabel.bind(this);
    this.chooseRandomlyFromList = this.chooseRandomlyFromList.bind(this);
    this.write = this.write.bind(this);
  }


  chooseRandomlyFromList(name) {
    let newName = String(name).toLowerCase()
    let potentialValue
    console.log(newName)
    if (newName == "zamawiający") {
      this.setState({
        label: name
      })
      potentialValue = [
        "Centralny Szpital Kliniczny MON",
        "Centrum Onkologii - Instytut im. M. Skłodowskiej-Curie",
        "Szpital Powiatowy im. J. Babińskiego. SPZZOZ",
        "Szpital Praski pw. Przemienienia Pańskiego SPZOZ",
        "Centralny Szpital Kliniczny MSWiA",
        "Wojewódzki Szpital Specjalistyczny",
        "Szpital Kolejowy im. dr. med. W. Roeflera",
        "Szpital Kliniczny Dzieciątka Jezus",
        "Inflancka. Szpital Ginekologiczno - Położniczy",
        "Międzyleski Szpital Specjalistyczny"
      ]
    }
    if (newName == "numer zamówienia" || newName == "nr zamówienia") {
      this.setState({
        label: name
      })
      potentialValue = [
        "IZK/Z1111/20/12345678",
        "IZK/Z1111/20/12345679",
        "IZK/Z1111/20/12345680",
        "ZZK/Z1111/20/12345681",
        "ZZK/Z1111/20/12345682",
        "ZZK/Z1111/20/12345683",
        "IZK/Z1111/20/12345684",
        "IZK/Z1111/20/12345685",
        "IZK/Z1111/20/12345686",
        "IZK/Z1111/20/12345687",
        "IZK/Z1111/20/12345688",
        "IZK/Z1111/20/12345689",
        "IZK/Z1111/20/12345690",
        "IZK/Z1111/20/12345691",
      ]
    }

    if (newName == "nazwa składnika") {
      this.setState({
        label: name
      })
      potentialValue = [
        "KPK",
        "UKP",
        "KKP-Af",
        "KKCz bez koż. l.-pł.",
        "KKCz/RW-bez koż. l.-pł.",
        "KKCz/RW",
        "UKKCz–Af",
        "UKKCz/RW",
        "MKKCz – glicerol 40%",
        "MKKP–Af.",
        "KG Af. –napromieniowany"
      ]
    }
    if (newName == "grupa krwi abo") {
      this.setState({
        label: name
      })
      potentialValue = [
        "AB Rh+(plus)",
        "AB Rh-(minus)",
        "A Rh-(minus)",
        "B Rh-(minus)",
        "B Rh+(plus)",
        "A Rh+(plus)",
        "O Rh+(plus)",
        "O Rh-(minus)"
      ]
    }




    if (newName == "wskazania do przetoczenia") {
      this.setState({
        label: name
      })
      potentialValue = [
        "Krwotok",
        "Anemia",
        "Białaczka",
        "Zatrucie",
        "Wypadek",
        "Krwotok po porodzie",

      ]
    }
    if (newName == "ilość") {
      this.setState({
        label: name
      })
      potentialValue = [
        "1",
        "1",
        "1",
        "2",
        "3",


      ]
    }
    if (newName == "status pozycji") {
      this.setState({
        label: name
      })
      potentialValue = [
        "---",
        "W przygotowaniu",


      ]
    }


    let i = Math.floor(Math.random() * potentialValue.length)

    this.setState({
      value: potentialValue[i]
    })
    console.log(potentialValue[i])
  }

  chooseRandomlyBasingOnLabel() {
    const { editDocument } = require("application");
    const { selection } = require("scenegraph");

    editDocument({ editLabel: "" },
      () => {
        selection.items.forEach((element) => {

          let levelOne = element.children
          levelOne.forEach(e1 => {
            let levelTwo = e1.children

            if (e1.name == "label / standard") {
              levelTwo.forEach(e2 => {
                this.chooseRandomlyFromList(e2.name)

              });
            }
          });

        })
      })
  }

  write() {
    const { editDocument } = require("application");
    const { selection } = require("scenegraph");

    editDocument({ editLabel: "" },
      () => {
        selection.items.forEach((element) => {

          element.text = this.state.value
          this.chooseRandomlyFromList(this.state.label)
        })
      })
  }



  readFromFiles() {
    let newName='zamawiający'
    let itemFromDataBase = Con.labelsAndValues.map(
      (item) => {
        if (item.label.toLowerCase() == newName) { return item }
      }
    )
    console.log(itemFromDataBase)
  }


  render() {
    return (

      <div className={Styles.flex}>

        <button onClick={this.readFromFiles}>Zczytaj dane z bazy danych</button>
        <button onClick={this.chooseRandomlyBasingOnLabel}>Zczytaj dane z komponentu</button>
        <button onClick={this.write}>Wpisz dane</button>
        <p className="small">{this.state.label}</p>
        <p className="small">{this.state.value}</p>
      </div>
    );
  }
}


module.exports = writeFromADatabase;