const React = require("react");
const Styles = require('./Styles.css')


class writeFromArray extends React.Component {
  constructor() {
    super();
    this.state = {
      dataToApplied: "",
      dataArray: []
    };
    this.writeDate = this.writeDate.bind(this);
    this.updateDataArray = this.updateDataArray.bind(this);

  }

  updateDataArray(e) {
    this.setState({
      dataToApplied: e.target.value
    })

  }

  writeFromArray2() {
    const { editDocument } = require("application");
    const { selection } = require("scenegraph");
    editDocument({ editLabel: "Change name" },
      () => {
        selection.items.forEach((element) => {
          if (element.constructor.name == "Text") {
            let day = randomInt(1, 27)
            if (day < 10) {
              day = "0" + day
            }
            let month = randomInt(1, 12)
            if (month < 10) {
              month = "0" + month
            }
            element.text = day + "." + month + ".2020"
          }
        })
      })
  }


  writeDate() {

    const { editDocument } = require("application");
    const { selection, Color } = require("scenegraph");


    var array = this.state.dataToApplied.split("$")
    var selectedItem = array[Math.floor(Math.random() * array.length)];





    editDocument({ editLabel: "Change position" },
      () => {
        selection.items.forEach((element) => {
          element.text = selectedItem
        })
      })
  }

  render() {
    return (

      <div className={Styles.flex}>

        <input value={this.state.dataToApplied} onChange={(e) => { this.updateDataArray(e) }}></input>

        <button onClick={this.writeDate}>Losuj</button>


      </div>
    );
  }
}


module.exports = writeFromArray;